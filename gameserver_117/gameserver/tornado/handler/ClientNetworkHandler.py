__author__ = 'fiduccia'

import tornado
import logging
from urllib.parse import urlparse
from tornado.options import options


class ClientNetworkHandler(tornado.web.WebSocketHandler):
    """
    Receive a message from the client
    """
    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.game = None

    def check_origin(self, origin):
        allowed = super().check_origin(origin)
        parsed = urlparse(origin.lower())
        matched = any(parsed.netloc == host for host in options.allowed_hosts)
        return options.debug or allowed or matched

    def open(self, game):
        self.game = game
        logging.info('open: ' + game)
        self.application.add_subscriber([self.game, 'all'], self)

    def on_message(self, message):
        logging.info('on_message: ' + message)
        self.application.broadcast(message, self.game, sender=self)

    def on_close(self):
        logging.info('on_close')
        self.application.remove_subscriber(self.game, self)
