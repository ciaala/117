/**
 * Created by fiduccia on 27/07/15.
 */
define(['engine/input/user-input'], function (userInput) {

    return {
        name: 'input',
        userInput: userInput,
    };
});