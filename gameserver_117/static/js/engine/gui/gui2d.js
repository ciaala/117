/**
 * Created by fiduccia on 28/07/15.
 */

define(['libraries/dat.gui'], function (dat) {
    return {
        name: 'gui2d',
        dat: dat,
    };

});