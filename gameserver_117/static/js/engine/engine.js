define(
    ['engine/audio/audio', 'engine/input/input', 'engine/renderer/renderer', 'engine/resource/resource-manager', 'engine/network/network'],
    function (audio, input, renderer, resourceManager, network) {
        "use strict";
        var engine;
        engine = {
            network: network,
            audio: audio,
            input: input,
            renderer: renderer,
            settings: {
                DEFAULT_HTML_CONTAINER_ID: 'engine_container',
                GAMESERVER: '192.168.1.136:9000/game',
                CHATAREA: 'ChatArea',
            },

            create_render: function () {

            },

            load_world: function () {

            },
            register_input_handler: function () {

            },
            update_game_resource: function () {
                //engine.resourceManager.update_game_resource();
            },
            connect: function () {
                engine.network.register(engine.settings.CHATAREA,engine.settings.GAMESERVER);
            },
            disconnect: function () {
                //engine.network.register(engine.settings.GAMESERVER);
            },
            start_loop: function () {

            },
            boot: function () {

                engine.connect();
                engine.update_game_resource();


                engine.create_render();
                //engine.connect();
                engine.register_input_handler();
                engine.start_loop();
            },
            login: function (username, password) {
                engine.network.login(username, password);
            },

            start: function () {
                //alert('loaded engine');
            }
        };
        engine.boot();
        return engine;
    }
);