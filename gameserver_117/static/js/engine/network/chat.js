
define(['./socket'], function(socket) {

    var chat = {
        _socket : {},
        _chatArea : {},
        send : function(message) {
            chat._socket.send(message);
        },
        register: function(chatAreaId, server) {
            var chatArea = window.document.getElementById(chatAreaId);
            if (chatArea !== null && chatArea !== undefined) {
                chat._chatArea = chatArea;

                chat._socket = socket.register(server + '/chat');
                chat._socket.on_open = function () {
                    console.log('chat on open');
                    chatArea.innerText += "Opening chat\n"
                };

                chat._socket.on_receive = function (message) {
                    console.log('Received message: ' + message.data);
                    chatArea.innerText += message.data + "\n";
                };
            } else {
                console.log('Unable to identify the chat area');
            }
        }

    };
    return chat;
})