/**
 * Created by fiduccia on 27/07/15.
 */
define(['./chat','./socket'], function(chat, socket) {
    "use strict";
    var network = {
        chat : chat,
        register : function (chatAreaId, server) {
            network.chat.register(chatAreaId, server);
            var gamePort = '1';
            var ws = new WebSocket('ws://' + server + '/' + gamePort);
            ws.onopen = function () {
                console.log('Connection is open');
                ws.send('ping');
            };
            ws.onmessage = function (message) {
                console.log('New message: ' + message.data);
                if (message.data == 'ping') {
                    ws.send('pong');
                }
            };
        },
    };
    return network;
});