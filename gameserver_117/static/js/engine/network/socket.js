/**
 * Created by crypt on 9/22/15.
 */
define(function() {
    var socket = {
        ws : {},
        register : function (server) {
            var gamePort = '1';
            socket.ws = new WebSocket('ws://' + server + '/' + gamePort);

            var app_socket = {
                send : function(message) {
                    socket.ws.send(message);
                },
                on_receive : function(message) {
                    console.log("Did you forget to register your message receiver.");
                },

                on_open : function() {
                    console.log("Did you forget to register your message receiver.");
                }
            };


            socket.ws.onopen = function () {
                app_socket.on_open();
            };
            socket.ws.onmessage = function (message) {
                app_socket.on_receive(message);
            };

            return app_socket;
        }
    };
    return socket;
})