/**
 * Created by fiduccia on 27/07/15.
 */
var engine;

requirejs(
    ["engine/engine"],
    function (_engine) {
        engine = _engine;
        engine.start();
    });
