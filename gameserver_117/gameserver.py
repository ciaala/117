
import logging
import signal
import time
import os
from urllib.parse import urlparse
from collections import defaultdict

from tornado.ioloop import IOLoop
from tornado.web import Application, StaticFileHandler
from tornado.websocket import WebSocketHandler, WebSocketClosedError
from tornado.options import define, parse_command_line, options
from tornado.httpserver import HTTPServer

define('debug', default=False, type=bool, help='Run id debug mode')
define('port', default=9000, type=int, help='Server port')
define('allowed_hosts', default=['localhost', '127.0.0.1', '::1', '192.168.1.136'], multiple=True,
       help='Allowed hosts for cross domain connections')
__author__ = 'fiduccia'


class ChatHandler(WebSocketHandler):

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.chat = None

    def check_origin(self, origin):
        allowed = super().check_origin(origin)
        parsed = urlparse(origin.lower())
        matched = any(parsed.netloc == host for host in options.allowed_hosts)
        logging.info('netloc ' + parsed.netloc + 'a:' + str(allowed) + ' m:' + str(matched))
        #return options.debug or allowed or matched
        return True

    def open(self, chat):
        self.chat = chat
        logging.info('open: ' + chat)
        self.application.add_subscriber([self.chat, 'all'], self)

    def on_message(self, message):
        logging.info('on_message: ' + message)
        self.application.broadcast(message, self.chat, sender=self)

    def on_close(self):
        logging.info('on_close')
        self.application.remove_subscriber(self.chat, self)


class GameHandler(WebSocketHandler):

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.game = None

    def check_origin(self, origin):
        allowed = super().check_origin(origin)
        parsed = urlparse(origin.lower())
        matched = any(parsed.netloc == host for host in options.allowed_hosts)
        #return options.debug or allowed or matched
        return True

    def open(self, game):
        self.game = game
        logging.info('open: ' + game)
        self.application.add_subscriber([self.game, 'all'], self)

    def on_message(self, message):
        logging.info('on_message: ' + message)
        self.application.broadcast(message, self.game, sender=self)

    def on_close(self):
        logging.info('on_close')
        self.application.remove_subscriber(self.game, self)


settings = {
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "cookie_secret": "__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
    "login_url": "/login",
    "xsrf_cookies": True,
}


class GameApplication(Application):
    def __init__(self, **kwargs):
        routes = [
            (r'/game/(?P<game>[0-9]+)', GameHandler),
            (r'/game/chat/(?P<chat>[0-9]+)', ChatHandler),
            (r'/(.*)', StaticFileHandler, dict(path='static', default_filename="index.html")),
        ]
        super().__init__(routes, **kwargs)
        self.subscriptions = defaultdict(list)

    def broadcast(self, message, channel=None, sender=None):
        if channel is None:
            for c in self.subscriptions.keys():
                self.broadcast(message, channel=c, sender=sender)
        else:
            peers = self.get_subscribers(channel)
            parameters = {'message': message, 'channel': channel, 'size': len(peers)}
            logging.info("Broadcasting message '{message}' to #{size} peers on channel '{channel}'".format(**parameters))
            for peer in peers:
                #if peer != sender:
                try:
                    peer.write_message(message)
                except WebSocketClosedError:
                    self.remove_subscriber(channel, peer)

    def add_subscriber(self, channel, subscriber):
        if type(channel) == list:
            for c in channel:
                self.subscriptions[c].append(subscriber)
        else:
            self.subscriptions[channel].append(subscriber)

    def remove_subscriber(self, channel, subscriber):
        self.subscriptions[channel].remove(subscriber)

    def get_subscribers(self, channel):
        return self.subscriptions[channel]


def shutdown(server):
    ioloop = IOLoop.instance()
    logging.info('stopping server')
    server.stop()

    def finalize():
        ioloop.stop()
        logging.info('Stopped')

    ioloop.add_timeout(time.time() + 1.5, finalize())


if __name__ == '__main__':
    parse_command_line()
    application = GameApplication(debug=options.debug)

    server = HTTPServer(application)
    server.listen(options.port)

    signal.signal(signal.SIGINT, lambda sig, frame: shutdown(server))
    logging.info('Starting server(pid:{}) on localhost:{}'.format(os.getpid(), options.port))
    IOLoop.instance().start()
