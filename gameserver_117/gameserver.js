/**
 * Created by fiduccia on 13/07/15.
 */
var socket = new WebSocket('ws://localhost:9000/1');
socket.onopen = function () {
    console.log('Connection is open');
    socket.send('ping');
};

socket.onmessage = function (message) {
    console.log('New message: ' + message.data);
    if (message.data == 'ping') {
        socket.send('pong');
    }
};
